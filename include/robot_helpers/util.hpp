#ifndef __ROBOT_HELPERS_UTIL_HPP__
#define __ROBOT_HELPERS_UTIL_HPP__

#include <robot_helpers/robot.hpp>

namespace robot_helpers {

// move arm so that the xtion looks at the table
bool moveXtionAboveTable(RobotArm &arm, const std::string &table_prefix, float dist = 1.0) ;

bool moveXtionAboveTray(RobotArm &arm, float dist);

}


#endif
