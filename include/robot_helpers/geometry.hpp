#ifndef __ROBOT_HELPERS_GEOMETRY_H__
#define __ROBOT_HELPERS_GEOMETRY_H__

#include <Eigen/Geometry>
#include <iostream>
#include <geometry_msgs/Pose.h>
#include <geometric_shapes/shapes.h>

using namespace Eigen;

namespace robot_helpers {

std::ostream &operator << (std::ostream &strm, const Eigen::Quaterniond &q) ;

// Conversion from RPY to Eigen quaternion

Eigen::Quaterniond quatFromRPY(double roll, double pitch, double yaw) ;
void rpyFromQuat(const Eigen::Quaterniond &q, double &roll, double &pitch, double &yaw) ;

// a rotation that will align the Z axis with the given direction

Eigen::Quaterniond lookAt(const Eigen::Vector3d &dir, double roll = 0.0) ;


geometry_msgs::Pose eigenPoseToROS(const Eigen::Vector3d &pos, const Eigen::Quaterniond &orient) ;
geometry_msgs::Pose eigenPoseToROS(const Eigen::Affine3d &pose_) ;
void RosPoseToEigenVectorQuaternion( Vector3d &pos,  Quaterniond &orient, geometry_msgs::Pose pose) ;
Eigen::Affine3d RosPoseToEigenAffine(  geometry_msgs::Pose pose) ;
Eigen::Vector3d RosPointToEigenVector3d(  geometry_msgs::Point p) ;

void eigenAffineToEigenVectorQuaternion( Eigen::Affine3d pose,  Vector3d &pos, Quaterniond &orient  ) ;
geometry_msgs::Pose eigenAffineToRos( Eigen::Affine3d pose ) ;
Affine3d eigenVectorQuaternionToAffine(const Vector3d &pos, const Quaterniond &q) ;
Quaterniond lookAtPoint(const Eigen::Vector3d pointSource, const Eigen::Vector3d pointTarget, double roll = 0) ;

// create a cone mesh
void makeSolidCone( shapes::Mesh  &mesh, double base, double height, int slices, int stacks ) ;

// create a camera frustum mesh
void makeCameraFrustum(shapes::Mesh  &mesh, double near_, double far_, double fovX, double fovY, bool makeSolid, bool addPrism) ;

geometry_msgs::Quaternion rotationMatrix4ToQuaternion(Eigen::Matrix4d matrix);
geometry_msgs::Quaternion rotationMatrix3ToQuaternion(Eigen::Matrix3d matrix);
Eigen::Quaterniond rotationMatrix4ToQuaterniond(Eigen::Matrix4d matrix);
Eigen::Quaterniond rotationMatrix3ToQuaterniond(Eigen::Matrix3d matrix);

geometry_msgs::Quaternion eigenQuaterniondToTfQuaternion( Eigen::Quaterniond q ) ;
Quaterniond TfQuaternionToEigenQuaterniond( geometry_msgs::Quaternion tfq ) ;


geometry_msgs::Pose eigenPoseToROS(const Eigen::Vector3d &pos, const Eigen::Quaterniond &orient) ;


} // namespace robot_helpers

#endif
