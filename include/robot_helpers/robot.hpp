#ifndef __ROBOT_HELPERS_ROBOT_HPP__
#define __ROBOT_HELPERS_ROBOT_HPP__

#include <clopema_robot/ClopemaRobotCommander.h>
#include <string>

namespace robot_helpers {

class RobotArm: public clopema_robot::ClopemaRobotCommander {
public:

    RobotArm(const std::string &arm) ;

    bool moveHome() ;

    Eigen::Affine3d getTipPose() ;

    bool planIK(const std::string &ee_name, const Eigen::Vector3d &pos, const Eigen::Quaterniond &q, ClopemaRobotCommander::Plan &plan);

    bool planTipIK(const Eigen::Vector3d &pos, const Eigen::Quaterniond &q, ClopemaRobotCommander::Plan &plan) ;
    bool moveTipIK(const Eigen::Vector3d &pos, const Eigen::Quaterniond &q);

    bool planXtionIK(const Eigen::Vector3d &pos, const Eigen::Quaterniond &q, ClopemaRobotCommander::Plan &plan) ;
    bool moveXtionIK(const Eigen::Vector3d &pos, const Eigen::Quaterniond &q) ;

    bool planTipCartesian(const Eigen::Vector3d &pos, ClopemaRobotCommander::Plan &plan);
    bool moveTipCartesian(const Eigen::Vector3d &pos);

    bool openGripper() ;
    bool closeGripper();
    bool isGripperOpen() ;

private:

    std::map<std::string, boost::shared_ptr<robot_state::AttachedBody> > attached_bodies_ ;
    std::string arm_name_ ;

};

class RobotArm1: public RobotArm {
public:
    RobotArm1(): RobotArm("r1") {}
};

class RobotArm2: public RobotArm {
public:
    RobotArm2(): RobotArm("r2") {}
};

class RobotGripper: public clopema_robot::ClopemaRobotCommander {
public:

    RobotGripper(const std::string &arm);

    bool changeGripperState(double value);

private:

    std::string arm_name_ ;

};

Eigen::Affine3d getTransform(const std::string &target, const std::string &base) ;

}

#endif
