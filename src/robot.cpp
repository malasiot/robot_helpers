#include <robot_helpers/robot.hpp>
#include <moveit/robot_state/conversions.h>
#include <robot_helpers/geometry.hpp>

#include <tf_conversions/tf_eigen.h>

#include <cassert>

using namespace std ;
using namespace Eigen ;

namespace robot_helpers {

RobotArm::RobotArm(const std::string &arm): arm_name_(arm), clopema_robot::ClopemaRobotCommander( arm + "_arm" ) {
    assert ( arm_name_ == "r1" || arm_name_ == "r2" )  ;
}


bool RobotArm::moveHome(){
    if ( arm_name_ == "r1" )
        return moveToNamedTarget(ClopemaRobotCommander::NAMED_TARGET_R1_ARM_HOME);
    else
        return moveToNamedTarget(ClopemaRobotCommander::NAMED_TARGET_R2_ARM_HOME);
}

bool RobotArm::planIK(const string &end_effector_link, const Eigen::Vector3d &pos, const Eigen::Quaterniond &q, ClopemaRobotCommander::Plan &plan )
{
    Affine3d pose = eigenVectorQuaternionToAffine(pos, q) ;

    setStartStateToCurrentState();
    if ( !setApproximateJointValueTarget(pose, end_effector_link) ) return false ;

    return (bool)ClopemaRobotCommander::plan(plan) ;
}

bool RobotArm::planTipCartesian(const Vector3d &pos, ClopemaRobotCommander::Plan &plan )
{
    clearPathConstraints();

    auto state = *getCurrentState() ;

    cartesian_path_print_err = true;

    Eigen::Affine3d pose = state.getFrameTransform(arm_name_ + "_ee") * Eigen::Translation3d(pos);

    moveit_msgs::RobotTrajectory traj ;
    double sol = computeCartesianPath(traj, {pose}, arm_name_ + "_ee", arm_name_ + "_arm"  ) ;


    if( sol != (double)0 ) {
        plan.trajectory_ = traj ;
        return true ;
    }
    else
        return false ;
}

bool RobotArm::moveTipCartesian(const Vector3d &pos) {
    moveit::planning_interface::MoveGroupInterface::Plan plan ;
    return planTipCartesian(pos, plan) && execute(plan) ;
}

bool RobotArm::openGripper() {
    return (bool)setGripperState(GRIPPER_OPEN) ;
}

bool RobotArm::closeGripper() {
    return (bool)setGripperState(GRIPPER_CLOSE) ;
}

bool RobotArm::isGripperOpen() {
    return fabs(getCurrentState()->getVariablePosition(arm_name_ + "_joint_grip") - 1.0) < 1.0e-5 ;
}

bool RobotArm::planTipIK(const Vector3d &pos, const Quaterniond &q, moveit::planning_interface::MoveGroupInterface::Plan &plan) {
    return planIK(arm_name_ + "_ee", pos, q, plan) ;
}

bool RobotArm::moveTipIK(const Vector3d &pos, const Quaterniond &q) {
    moveit::planning_interface::MoveGroupInterface::Plan plan ;
    return planTipIK(pos, q, plan) && execute(plan) ;
}

bool RobotArm::planXtionIK(const Vector3d &pos, const Quaterniond &q, moveit::planning_interface::MoveGroupInterface::Plan &plan) {
    robot_state::RobotState rs(*getCurrentState());

    Affine3d pose = eigenVectorQuaternionToAffine(pos, q) ;

    const robot_state::JointModelGroup *jmg = rs.getJointModelGroup(arm_name_ + "_xtion") ;

    if ( !rs.setFromIK(jmg, pose) )  return false ;

    vector<string> names ;
    vector<double> values ;
    rs.copyJointGroupPositions(jmg, values);

    names = jmg->getVariableNames() ;

    for(int i=0 ; i<names.size() ; i++ )
        setJointValueTarget(names[i], values[i]) ;

    return  (bool)ClopemaRobotCommander::plan(plan) ;
}

bool RobotArm::moveXtionIK(const Vector3d &pos, const Quaterniond &q)
{
    moveit::planning_interface::MoveGroupInterface::Plan plan ;
    return planXtionIK(pos, q, plan) && execute(plan) ;
}


Affine3d RobotArm::getTipPose()
{
    geometry_msgs::PoseStamped pose = getCurrentPose(arm_name_ + "_ee") ;

    return RosPoseToEigenAffine(pose.pose) ;
}

RobotGripper::RobotGripper(const std::string &arm): arm_name_(arm), clopema_robot::ClopemaRobotCommander( arm + "_gripper" ) {
    assert ( arm_name_ == "r1" || arm_name_ == "r2" )  ;
}

bool RobotGripper::changeGripperState(double value)
{
    moveit::planning_interface::MoveGroupInterface::Plan plan ;
    if (setJointValueTarget("r2_joint_grip", value)){
        moveit::planning_interface::MoveGroupInterface::plan(plan);
        if (execute(plan))
            return true;
        else
            return false;
    }
    else{
        return false;
    }

}

Affine3d getTransform(const string &target, const string &base)
{
   Affine3d pose ;
   pose.setIdentity() ;

   if ( target == base ) return pose ;

   tf::TransformListener listener;
   tf::StampedTransform transform ;

    try {
        if ( listener.waitForTransform(target, base, ros::Time(0), ros::Duration(10) ) ) {
            listener.lookupTransform(target, base, ros::Time(0), transform);
            tf::transformTFToEigen(transform, pose);
            return pose ;
        }

    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
        return pose ;
    }

    return pose ;
}

}
