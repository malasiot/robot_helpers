#include <robot_helpers/util.hpp>
#include <robot_helpers/robot.hpp>
#include <robot_helpers/geometry.hpp>

using namespace std ;
using namespace Eigen ;

namespace robot_helpers {

bool moveXtionAboveTable(RobotArm &arm, const std::string &table_prefix, float dist) {
    Affine3d tr = getTransform(table_prefix + "_desk", "base_link") ;
    tr = tr.inverse() ;
    Vector3d parking_pose = tr.translation() ;
    parking_pose.z() += dist ;
   // double table_height = tr.translation().z() ;

    RobotArm::Plan plan ;
    if ( arm.planXtionIK(parking_pose, lookAt(Vector3d(0, 0, -1)), plan) )  {
           arm.execute(plan) ;
           return true ;
    }

    return false ;
}

bool moveXtionAboveTray(RobotArm &arm, float dist) {
    Affine3d tr = getTransform("tray_bottom", "base_link") ;
    tr = tr.inverse() ;
    Vector3d parking_pose = tr.translation() ;
    parking_pose.z() += dist ;
   // double table_height = tr.translation().z() ;

    RobotArm::Plan plan ;
    if ( arm.planXtionIK(parking_pose, lookAt(Vector3d(0, 0, -1)), plan) )  {
           arm.execute(plan) ;
           return true ;
    }

    return false ;
}

}
