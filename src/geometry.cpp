#include <robot_helpers/geometry.hpp>
#include <iostream>
#include <Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>


#include <geometric_shapes/shape_operations.h>

using namespace std ;
using namespace Eigen ;

namespace robot_helpers {

ostream &operator << (ostream &strm, const Quaterniond &q)
{
    cout << "[ " << q.x() << ' ' << q.y() << ' ' << q.z() << ' ' << q.w() << " ]";

    return strm ;
}


Quaterniond quatFromRPY(double roll, double pitch, double yaw)
{
    Quaterniond q ;

    q = AngleAxisd(yaw, Eigen::Vector3d::UnitZ()) * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX());

    return q ;
}

void rpyFromQuat(const Quaterniond &q, double &roll, double &pitch, double &yaw)
{
    Matrix3d r = q.toRotationMatrix() ;
    Vector3d euler = r.eulerAngles(2, 1, 0) ;
    yaw = euler.x() ;
    pitch = euler.y() ;
    roll = euler.z() ;

 //   pitch = atan2( 2*(q.y()*q.z() + q.w()*q.x()), q.w()*q.w() - q.x() * q.x() - q.y() * q.y() + q.z() * q.z()) ;
 //   yaw = asin(-2*(q.x()*q.z() - q.w()*q.y()) ) ;
 //   roll = atan2(2*(q.x()*q.y() + q.w()*q.z()), q.w()*q.w() + q.x() * q.x() - q.y() * q.y() - q.z() * q.z()) ;


}

Quaterniond lookAt(const Eigen::Vector3d &dir, double roll)
{
     Vector3d nz = dir, na, nb ;
     nz.normalize() ;

     double q = sqrt(nz.x() * nz.x() + nz.y() * nz.y()) ;

     if ( q < 1.0e-4 )
     {
         na = Vector3d(1, 0, 0) ;
         nb = nz.cross(na) ;
     }
     else {
         na = Vector3d(-nz.y()/q, nz.x()/q, 0) ;
         nb = Vector3d(-nz.x() * nz.z()/q, -nz.y() * nz.z()/q, q) ;
     }

     Matrix3d r ;
     r << na, nb, nz ;

     return Quaterniond(r) * AngleAxisd(roll, Eigen::Vector3d::UnitZ()) ;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////


// hacked from freeglut

static void fghCircleTable(double **sint,double **cost,const int n)
{
    int i;

    /* Table size, the sign of n flips the circle direction */

    const int size = abs(n);

    /* Determine the angle between samples */

    const double angle = 2*M_PI/(double)( ( n == 0 ) ? 1 : n );

    /* Allocate memory for n samples, plus duplicate of first entry at the end */

    *sint = (double *) calloc(sizeof(double), size+1);
    *cost = (double *) calloc(sizeof(double), size+1);

    /* Bail out if memory allocation fails, fgError never returns */

    if (!(*sint) || !(*cost))
    {
        free(*sint);
        free(*cost);

    }

    /* Compute cos and sin around the circle */

    (*sint)[0] = 0.0;
    (*cost)[0] = 1.0;

    for (i=1; i<size; i++)
    {
        (*sint)[i] = sin(angle*i);
        (*cost)[i] = cos(angle*i);
    }

    /* Last sample is duplicate of the first */

    (*sint)[size] = (*sint)[0];
    (*cost)[size] = (*cost)[0];
}


void makeSolidCone( shapes::Mesh  &mesh, double base, double height, int slices, int stacks )
{
    int i,j;

    /* Step in z and radius as stacks are drawn. */

    double z0, z1;
    double r0, r1;

    const double zStep = height / ( ( stacks > 0 ) ? stacks : 1 );
    const double rStep = base / ( ( stacks > 0 ) ? stacks : 1 );

    /* Scaling factors for vertex normals */

    const double cosn = ( height / sqrt ( height * height + base * base ));
    const double sinn = ( base   / sqrt ( height * height + base * base ));

    /* Pre-computed circle */

    double *sint,*cost;


    fghCircleTable(&sint,&cost,-slices);

    /* Cover the circular base with a triangle fan... */

    z0 = -height ;

    r0 = base;
    r1 = r0 - rStep;

    int maxVert = 6*(slices * stacks + 10) ;

    mesh.vertex_count = 0 ;
    mesh.vertices = new double [maxVert] ;
    mesh.triangles = new unsigned int [maxVert] ;
    mesh.vertex_normals =  new double [maxVert] ;

    int vc = 0, nc = 0, tc = 0 ;

    // make bottom faces

    mesh.vertices[vc++] = 0.0 ;
    mesh.vertices[vc++] = 0.0 ;
    mesh.vertices[vc++] = z0 ;


    mesh.vertex_count ++ ;

    for (j=0; j<slices; j++ )
    {
        mesh.vertices[vc++] = cost[j]*r0 ;
        mesh.vertices[vc++] = sint[j]*r0 ;
        mesh.vertices[vc++] = z0 ;

        mesh.vertex_count ++ ;
    }

#define CYCLE(a) (((a)==slices+1) ? 1 : (a))

    for (j=0; j<slices; j++ )
    {

        mesh.triangles[tc++] = j+1 ;
        mesh.triangles[tc++] = 0 ;
        mesh.triangles[tc++] = CYCLE(j+2) ;


        mesh.vertex_normals[nc++] = 0.0 ;
        mesh.vertex_normals[nc++] = 0.0 ;
        mesh.vertex_normals[nc++] = 1 ;

        mesh.triangle_count ++ ;
    }


    /* Cover each stack with a quad strip, except the top stack */

    r1 = r0 ;
    z1 = z0 ;

    for( i=1; i<stacks-1; i++ )
    {
        r1 -= rStep ;
        z1 += zStep ;

        for(j=0; j<slices; j++)
        {
            mesh.vertices[vc++] = cost[j]*r1 ;
            mesh.vertices[vc++] = sint[j]*r1 ;
            mesh.vertices[vc++] = z1 ;

            mesh.vertex_count ++ ;
        }



        for(j=0; j<slices; j++)
        {

            mesh.triangles[tc++] = (i -1)*(slices) + j + 1 ;
            mesh.triangles[tc++] = (i -1)*(slices) + CYCLE(j+2);
            mesh.triangles[tc++] = i * (slices)  + CYCLE(j+2) ;
            mesh.triangle_count ++ ;

            mesh.vertex_normals[nc++] = cost[j]*cosn ;
            mesh.vertex_normals[nc++] = sint[j]*cosn ;
            mesh.vertex_normals[nc++] = sinn ;


            mesh.triangles[tc++] = (i -1)*(slices) + j + 1 ;
            mesh.triangles[tc++] = i * (slices) + CYCLE(j+2) ;
            mesh.triangles[tc++] = i * (slices)  + j + 1 ;
            mesh.triangle_count ++ ;

            mesh.vertex_normals[nc++] = cost[j]*cosn ;
            mesh.vertex_normals[nc++] = sint[j]*cosn ;
            mesh.vertex_normals[nc++] = sinn ;

        }
     }

    mesh.vertices[vc++] = 0 ;
    mesh.vertices[vc++] = 0 ;
    mesh.vertices[vc++] = 0 ;

    mesh.vertex_count ++ ;

    for(j=0; j<slices; j++)
    {

        mesh.triangles[tc++] = mesh.vertex_count -1 - slices + j + 1 ;
        mesh.triangles[tc++] = mesh.vertex_count -1 - slices + CYCLE(j+2) ;
        mesh.triangles[tc++] = mesh.vertex_count -1 ;

        mesh.vertex_normals[nc++] = cost[j]*cosn ;
        mesh.vertex_normals[nc++] = sint[j]*cosn ;
        mesh.vertex_normals[nc++] = sinn ;

        mesh.triangle_count ++ ;
    }

    /* Release sin and cos tables */

    free(sint);
    free(cost);
}


void makeCameraFrustum(shapes::Mesh  &mesh, double near_, double far_, double fovX, double fovY, bool makeSolid, bool addPrism)
{
    double ax = tan(fovX/2) ;
    double ay = tan(fovY/2) ;

    double nx = near_ * ax ;
    double ny = near_ * ay ;
    double fx = far_ * ax ;
    double fy = far_ * ay ;

    Vector3d p[9] ;

    p[0] = Vector3d(0, 0, 0) ; // origin

    p[1] = Vector3d( -nx, -ny, near_ );
    p[2] = Vector3d( nx, -ny, near_ );
    p[3] = Vector3d( nx, ny, near_ );
    p[4] = Vector3d( -nx, ny, near_ );

    p[5] = Vector3d( -fx, -fy, far_ );
    p[6] = Vector3d( fx, -fy, far_ );
    p[7] = Vector3d( fx, fy, far_ );
    p[8] = Vector3d( -fx, fy, far_ );

    mesh.vertex_count = 9 ;
    mesh.vertices = new double [3 * mesh.vertex_count] ;

    mesh.triangle_count = 8 ;

    if ( makeSolid ) {
        mesh.triangle_count += 2 ;
        if ( !addPrism )
            mesh.triangle_count += 2 ;
    }

    if ( addPrism ) mesh.triangle_count += 4 ;


    mesh.triangles = new unsigned int [mesh.triangle_count * 3] ;

    for(int i=0, k=0 ; i<9 ; i++  )
    {
        mesh.vertices[k++] = p[i].x() ;
        mesh.vertices[k++] = p[i].y() ;
        mesh.vertices[k++] = p[i].z() ;
    }

#define ADD_TRIANGLE(v1, v2, v3)\
    mesh.triangles[k++] = v1 ;\
    mesh.triangles[k++] = v2 ;\
    mesh.triangles[k++] = v3 ;

    int k=0 ;

    ADD_TRIANGLE(1, 5, 4) ;
    ADD_TRIANGLE(4, 5, 8) ;
    ADD_TRIANGLE(1, 5, 6) ;
    ADD_TRIANGLE(1, 6, 2) ;
    ADD_TRIANGLE(2, 6, 3) ;
    ADD_TRIANGLE(3, 6, 7) ;
    ADD_TRIANGLE(4, 8, 7) ;
    ADD_TRIANGLE(4, 7, 3) ;

    if ( makeSolid )
    {
        ADD_TRIANGLE(5, 6, 7) ;
        ADD_TRIANGLE(5, 7, 8) ;

        if ( !addPrism )
        {
            ADD_TRIANGLE(1, 2, 3) ;
            ADD_TRIANGLE(1, 3, 4) ;
        }
    }

    if ( addPrism )
    {
        ADD_TRIANGLE(0, 1, 2) ;
        ADD_TRIANGLE(0, 2, 3) ;
        ADD_TRIANGLE(0, 3, 4) ;
        ADD_TRIANGLE(0, 4, 1) ;
    }

    mesh.vertex_normals = 0 ;
}



//Calculates the tf quaternion of a 4d rotatation matrix
geometry_msgs::Quaternion rotationMatrix4ToQuaternion(Eigen::Matrix4d matrix){

    float roll , pitch, yaw;

    roll= atan2f(matrix(2, 1),matrix(2, 2) );
    pitch= atan2f(-matrix(2,0),sqrt(pow(matrix(2, 2),2)+pow(matrix(2, 1),2)));
    yaw= atan2f(matrix(1, 0),matrix(0, 0));
    return tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw );
}

//Calculates the tf quaternion of a 3d rotatation matrix
geometry_msgs::Quaternion rotationMatrix3ToQuaternion(Eigen::Matrix3d matrix){

    float roll , pitch, yaw;

    roll= atan2f(matrix(2, 1),matrix(2, 2) );

    float c2 = sqrt(matrix(0, 0)*matrix(0, 0)+matrix(1, 0)*matrix(1, 0));
    pitch= atan2f(-matrix(2,0),c2);

    float s1 = sin(roll);
    float c1 = cos(roll);
    yaw= atan2f((s1*matrix(0,2) - c1*matrix(0,1)), (c1*matrix(1,1) - s1*matrix(1,2)));

    return tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw );
}

//Calculates the eigen quaternion of a 4d rotatation matrix

Eigen::Quaterniond rotationMatrix4ToQuaterniond(Eigen::Matrix4d matrix){

    float roll , pitch, yaw;

    roll= atan2f(matrix(2, 1),matrix(2, 2) );
    pitch= atan2f(-matrix(2,0),sqrt(pow(matrix(2, 2),2)+pow(matrix(2, 1),2)));
    yaw= atan2f(matrix(1, 0),matrix(0, 0));

    geometry_msgs::Quaternion tfq = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw );
    Eigen::Quaterniond q;

    q.x()= tfq.x;
    q.y()= tfq.y;
    q.z()= tfq.z;
    q.w()= tfq.w;
    return q;
}

//Calculates the eigen quaternion of a 3d rotatation matrix
Eigen::Quaterniond rotationMatrix3ToQuaterniond(Eigen::Matrix3d matrix){

    float roll , pitch, yaw;

    roll= atan2f(matrix(2, 1),matrix(2, 2) );

    float c2 = sqrt(matrix(0, 0)*matrix(0, 0)+matrix(1, 0)*matrix(1, 0));
    pitch= atan2f(-matrix(2,0),c2);

    float s1 = sin(roll);
    float c1 = cos(roll);
    yaw= atan2f((s1*matrix(0,2) - c1*matrix(0,1)), (c1*matrix(1,1) - s1*matrix(1,2)));

    geometry_msgs::Quaternion tfq = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw );
    Eigen::Quaterniond q;

    q.x()= tfq.x;
    q.y()= tfq.y;
    q.z()= tfq.z;
    q.w()= tfq.w;
    return q;
}





geometry_msgs::Quaternion eigenQuaterniondToTfQuaternion( Quaterniond q ){

    geometry_msgs::Quaternion tfq;

    tfq.x = q.x();
    tfq.y = q.y();
    tfq.z = q.z();
    tfq.w = q.w();

    return tfq;

}

Quaterniond TfQuaternionToEigenQuaterniond( geometry_msgs::Quaternion tfq ){

    Quaterniond q;

    q.x() = tfq.x ;
    q.y() = tfq.y ;
    q.z() = tfq.z ;
    q.w() = tfq.w ;

    return q;

}
geometry_msgs::Pose eigenPoseToROS(const Vector3d &pos, const Quaterniond &orient)
{
    geometry_msgs::Pose pose ;

    pose.position.x = pos.x() ;
    pose.position.y = pos.y() ;
    pose.position.z = pos.z() ;

    pose.orientation.x = orient.x() ;
    pose.orientation.y = orient.y() ;
    pose.orientation.z = orient.z() ;
    pose.orientation.w = orient.w() ;

    return pose ;

}

geometry_msgs::Pose eigenPoseToROS(const Affine3d &pose_)
{

    Vector3d pos = pose_.translation() ;
    Quaterniond orient(pose_.rotation()) ;

    return eigenPoseToROS(pos, orient) ;

}

void RosPoseToEigenVectorQuaternion( Vector3d &pos,  Quaterniond &orient, geometry_msgs::Pose pose)
{
    pos.x() = pose.position.x ;
    pos.y() = pose.position.y ;
    pos.z() = pose.position.z ;

    orient.x() = pose.orientation.x ;
    orient.y() = pose.orientation.y ;
    orient.z() =  pose.orientation.z ;
    orient.w() = pose.orientation.w ;

}

Eigen::Affine3d RosPoseToEigenAffine(  geometry_msgs::Pose pose)
{

    Vector3d pos ;
    Quaterniond orient ;
    RosPoseToEigenVectorQuaternion(pos, orient, pose) ;
    Eigen::Affine3d trans ;
    trans.translation() = pos ;
    trans.linear() = orient.toRotationMatrix() ;
    return trans ;

}


Eigen::Vector3d RosPointToEigenVector3d(  geometry_msgs::Point p)
{
    Vector3d pos(p.x, p.y, p.z) ;
    return pos ;
}

void eigenAffineToEigenVectorQuaternion( Eigen::Affine3d pose,  Vector3d &pos, Quaterniond &orient  )
{

    pos = pose.translation() ;
    orient = pose.rotation() ;

}

geometry_msgs::Pose eigenAffineToRos( Eigen::Affine3d pose )
{
    Vector3d pos;
    Quaterniond orient ;

    pos = pose.translation() ;
    orient = pose.rotation() ;

    return eigenPoseToROS(pos, orient) ;
}

Affine3d eigenVectorQuaternionToAffine(const Vector3d &pos, const Quaterniond &q){

    Affine3d trans ;
    trans.translation() = pos ;
    trans.linear() = q.toRotationMatrix() ;

    return trans ;
}

Quaterniond lookAtPoint(const Eigen::Vector3d pointSource, const Eigen::Vector3d pointTarget, double roll){



    Vector3d nz = pointTarget - pointSource, na, nb ;

    nz.normalize() ;

    double q = sqrt(nz.x() * nz.x() + nz.y() * nz.y()) ;

    if ( q < 1.0e-4 )
    {
        na = Vector3d(1, 0, 0) ;
        nb = nz.cross(na) ;
    }
    else {
        na = Vector3d(-nz.y()/q, nz.x()/q, 0) ;
        nb = Vector3d(-nz.x() * nz.z()/q, -nz.y() * nz.z()/q, q) ;
    }

    Matrix3d r ;
    r << na, nb, nz ;

    return Quaterniond(r) * AngleAxisd(roll, Eigen::Vector3d::UnitZ()) ;
}





} // namespace robot_helpers
