#include <clopema_robot/robot_commander.h>
#include <ros/ros.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>


using namespace std;
using namespace Eigen ;

float getRandomRotation(){

    float rad ;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    srand (tv.tv_usec);
    return rad = (float)(rand() %628  + 1)/100.0;

}


Quaterniond lookAt(const Eigen::Vector3d &dir, double roll)
{
     Vector3d nz = dir, na, nb ;
     nz.normalize() ;

     double q = sqrt(nz.x() * nz.x() + nz.y() * nz.y()) ;

     if ( q < 1.0e-4 )
     {
         na = Vector3d(1, 0, 0) ;
         nb = nz.cross(na) ;
     }
     else {
         na = Vector3d(-nz.y()/q, nz.x()/q, 0) ;
         nb = Vector3d(-nz.x() * nz.z()/q, -nz.y() * nz.z()/q, q) ;
     }

     Matrix3d r ;
     r << na, nb, nz ;

     return Quaterniond(r) * AngleAxisd(roll, Eigen::Vector3d::UnitZ()) ;
}

geometry_msgs::Pose eigenPoseToROS(const Vector3d &pos, const Quaterniond &orient)
{
    geometry_msgs::Pose pose ;

    pose.position.x = pos.x() ;
    pose.position.y = pos.y() ;
    pose.position.z = pos.z() ;

    pose.orientation.x = orient.x() ;
    pose.orientation.y = orient.y() ;
    pose.orientation.z = orient.z() ;
    pose.orientation.w = orient.w() ;

    return pose ;

}

bool moveArms(const Eigen::Vector3d pose1, const Eigen::Vector3d pose2,  const Eigen::Quaterniond q1, const Eigen::Quaterniond q2, const std::string armName1, const std::string armName2){

    clopema_robot::ClopemaRobotCommander g("arms");
    g.setRobotSpeed(0.2);
    robot_state::RobotState rs(*g.getCurrentState());

    geometry_msgs::Pose pose;

    pose = eigenPoseToROS(pose1, q1) ;
    rs.setFromIK(rs.getJointModelGroup(armName1 + "_arm"), pose, armName1 + "_ee");

    pose = eigenPoseToROS(pose2, q2) ;
    rs.setFromIK(rs.getJointModelGroup(armName2 + "_arm"), pose, armName2 + "_ee");

    g.setJointValueTarget(rs);

    g.move();


}

bool moveArmsNoTearing( const Eigen::Vector3d pose1, const Eigen::Vector3d pose2,  const Eigen::Quaterniond q1, const Eigen::Quaterniond q2,
                                              const float radius, const float speed,  const std::string armName1, const std::string armName2){

    clopema_robot::ClopemaRobotCommander g("arms");
    g.setRobotSpeed(0.2);
    moveit_msgs::PositionConstraint pc;
    { //Set position constraint
        moveit_msgs::BoundingVolume reg;
        shape_msgs::SolidPrimitive prim;
        prim.type = prim.SPHERE;
        prim.dimensions.resize(1);
        prim.dimensions[0] = radius ;

        geometry_msgs::Pose pose;
        pose.position.x = 0;
        pose.position.y = 0;
        pose.position.z = 0;
        pose.orientation.w = 1.0;

        reg.primitives.push_back(prim);
        reg.primitive_poses.push_back(pose);
        pc.header.stamp = ros::Time::now();
        pc.constraint_region = reg;
        pc.link_name = "r1_ee";
        pc.header.frame_id = "r2_ee";
        pc.weight = 1.0;
    }
    moveit_msgs::Constraints c;
    c.name = "no_tearing";
    c.position_constraints.push_back(pc);
    g.setPathConstraints(c) ;

    robot_state::RobotState rs(*g.getCurrentState());

    geometry_msgs::Pose pose;

    pose = eigenPoseToROS(pose1, q1) ;
    rs.setFromIK(rs.getJointModelGroup(armName1 + "_arm"), pose, armName1 + "_ee");

    pose = eigenPoseToROS(pose2, q2) ;
    rs.setFromIK(rs.getJointModelGroup(armName2 + "_arm"), pose, armName2 + "_ee");

    g.setJointValueTarget(rs);

   return g.move();

}

//int main(int argc, char *argv[])
//{
//    ros::init(argc, argv, "test_grippers") ;
//    ros::NodeHandle nh;
//    ros::AsyncSpinner spinner(4) ;
//    spinner.start() ;


//    Vector3d  pose1(-0.1, -0.8,  1.6), pose2 ;
//    pose2 = pose1 ;
//    pose2.x() = pose1.x() + 0.3 ;
//    pose2.z() = pose1.z() - 0.5 ;

//    Quaterniond q1, q2 ;
//    q2 = lookAt(Vector3d(-1, 0, 0), 0) ;

//    float rot ;
//    clopema_robot::ClopemaRobotCommander g("arms");
//    for(unsigned int i = 0 ; i < 5 ; i++){

//        q1 = lookAt(Vector3d(0, 0, -1), 0) ;
//        moveArms(pose1, pose2, q1, q2, "r1", "r2") ;

//        rot = getRandomRotation();
//        cout << " rotation of gripper = " << rot << " rad." << endl;
//        q1 = lookAt(Vector3d(0, 0, -1), -rot) ;

//        moveArms(pose1, pose2, q1, q2, "r1", "r2") ;
//        moveArmsNoTearing(Vector3d(0, -1, 1.3), Vector3d(-0.45, -1, 1.3), lookAt(Vector3d(-1, 0, -1), 0), lookAt(Vector3d(1, 0, -1), M_PI), 0.46, 0.1, "r2", "r1") ;

//    }

//    ros::shutdown() ;
//    return 0;

//}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_grippers") ;
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;


    Vector3d  pose2(-0.23, -0.8, 1.58 ), pose1 ;
    pose1 = pose2 ;
    pose1.x() = pose2.x() - 0.3 ;
    pose1.z() = pose2.z() - 0.5 ;

    Quaterniond q1, q2 ;
    q1 = lookAt(Vector3d(1, 0, 0), 0) ;

    float rot ;
    clopema_robot::ClopemaRobotCommander g("arms");
    for(unsigned int i = 0 ; i < 5 ; i++){

        q2 = lookAt(Vector3d(0, 0, -1), 0) ;
        moveArms(pose1, pose2, q1, q2, "r1", "r2") ;

        rot = getRandomRotation();
        cout << " rotation of gripper = " << rot << " rad." << endl;
        q2 = lookAt(Vector3d(0, 0, -1), -rot) ;

        moveArms(pose1, pose2, q1, q2, "r1", "r2") ;
        moveArmsNoTearing(Vector3d(0, -1, 1.3), Vector3d(-0.45, -1, 1.3), lookAt(Vector3d(-1, 0, -1), M_PI), lookAt(Vector3d(1, 0, -1), 0), 0.46, 0.1, "r2", "r1") ;

    }

    ros::shutdown() ;
    return 0;

}


