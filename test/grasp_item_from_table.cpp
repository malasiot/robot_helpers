
#include <ros/ros.h>
#include <robot_helpers/Robot2.h>
#include <robot_helpers/Utils2.h>
#include <sstream>
#include <robot_helpers/Geometry.h>
#include <fstream>
#include <cstdio>
#include <camera_helpers/OpenNIServiceClient.h>


#define MAX_SPEED  0.2
#define MID_SPEED  0.1
#define MIN_SPEED  0.01

using namespace std;
using namespace robot_helpers ;
using namespace Eigen ;





int main(int argc, char *argv[])
{

    ros::init(argc, argv, "planning") ;

    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    Robot2 rb("r1") ;

    rb.getCommander()->setRobotSpeed(0.07);

    rb.moveHomeArm() ;
    Eigen::Vector3d pose ;
    pose << 0, -1.2, 1 ;
    Eigen::Quaterniond q ;
    q = lookAt(Eigen::Vector3d(0,0,-1), M_PI) ;
    rb.moveGripperToPose(pose, q) ;

    pose << 0, -1.2, 0.73 ;
    rb.moveGripperToPose(pose, q) ;
    rb.setGripperState("r1", false);

    pose << 0, -1.2, 1 ;
    rb.moveGripperToPose(pose, q) ;


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;


    pose << -0.59, -1.1, 0.97 ;
    rb.moveGripperToPose(pose, q) ;

    rb.setGripperState("r1", true);


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;



    /////////////////////////////////////



    pose << 0.2, -1, 1 ;
    q = lookAt(Eigen::Vector3d(0,0,-1), M_PI/2) ;
    rb.moveGripperToPose(pose, q) ;

    pose << 0.2, -1, 0.78 ;
    rb.moveGripperToPose(pose, q) ;
    rb.setGripperState("r1", false);

    pose << 0.2, -1, 1 ;
    rb.moveGripperToPose(pose, q) ;


    q = lookAt(Eigen::Vector3d(0,0,-1)) ;


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;


    pose << -0.59, -1.1, 0.97 ;
    rb.moveGripperToPose(pose, q) ;

    rb.setGripperState("r1", true);


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;

    rb.moveHomeArm() ;


 ////////////////////



    pose << -0.1, -1.1, 1 ;
    q = lookAt(Eigen::Vector3d(0,0,-1), -M_PI/4) ;
    rb.moveGripperToPose(pose, q) ;

    pose << -0.1, -1.1, 0.78 ;
    rb.moveGripperToPose(pose, q) ;
    rb.setGripperState("r1", false);

    pose << -0.1, -1.1, 1 ;
    rb.moveGripperToPose(pose, q) ;


    q = lookAt(Eigen::Vector3d(0,0,-1)) ;


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;


    pose << -0.59, -1.1, 0.97 ;
    rb.moveGripperToPose(pose, q) ;

    rb.setGripperState("r1", true);


    pose << -0.59, -1.1, 1.1 ;
    rb.moveGripperToPose(pose, q) ;

    rb.moveHomeArm() ;



}

