#include <ros/ros.h>
#include <robot_helpers/Robot2.h>
#include <robot_helpers/Utils2.h>
#include <robot_helpers/Geometry.h>

#include <certh_layontable/LayOnTable.h>

using namespace std;
using namespace robot_helpers ;
using namespace Eigen ;


float getRandomDistance(){

    while(1){
        float dist ;
        struct timeval tv;
        gettimeofday(&tv,NULL);
        srand (tv.tv_usec);
        dist = ((float) rand() / (RAND_MAX)) ;

        if (dist > 0.3 && dist < 0.7)
            return dist ;
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "planning") ;
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    Robot2 rb ;
    float clothLength ;

    clothLength = 0.37 ;//getRandomDistance() ;
    moveArms(rb, Vector3d(-clothLength / 2.0, -0.6, 1.3), Vector3d( clothLength / 2.0, -0.6, 1.3), lookAt(Vector3d(1, 0, -1)) , lookAt(Vector3d(-1, 0, -1)) ) ;

    rb.setGripperState("r1", false);
    rb.setGripperState("r2", false);

    ros::service::waitForService("/lay_on_table") ;
    ros::ServiceClient client = nh.serviceClient<certh_layontable::LayOnTable>("/lay_on_table");

    certh_layontable::LayOnTable::Request req ;
    certh_layontable::LayOnTable::Request res ;

    req.time_out = 5.0 ;

    client.call(req, res) ;


    rb.moveHomeArms();




}
