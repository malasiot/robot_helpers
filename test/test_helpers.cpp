#include <robot_helpers/robot.hpp>
#include <robot_helpers/geometry.hpp>
#include <robot_helpers/util.hpp>

using namespace Eigen;
using namespace robot_helpers ;
using namespace std ;

void test1() {

    string arm_name = "r2" ;
    RobotArm arm(arm_name) ;

    arm.moveHome();

    RobotGripper rg(arm_name);

    clopema_robot::ClopemaRobotCommander::Plan plan ;

    if (rg.changeGripperState(0.05))
        cout << "Gripper state changed" << endl;
    else
        cout << "Error" << endl;

    ros::Duration(5).sleep();

    rg.changeGripperState(0.2);

    ros::Duration(5).sleep();

    rg.changeGripperState(0.3);

    ros::Duration(5).sleep();

    rg.changeGripperState(0.5);
}


void test2() {

    string arm_name = "r2";
    RobotArm arm(arm_name);

}


void test3() {
    RobotArm2 r ;

    r.moveHome() ;

    clopema_robot::ClopemaRobotCommander::Plan plan ;
    if ( r.planTipIK(Vector3d(0, -1.0, 0.8), robot_helpers::lookAt(Vector3d(0, 0, -1), M_PI/2), plan) )
        r.execute(plan) ;


    Affine3d pose1 = getTransform("r2_ee", "base_link") ;
    Affine3d pose2 = r.getTipPose() ;
    cout << pose1.translation().adjoint() << ' ' << pose2.translation().adjoint() << endl ;
    Vector3d pp = pose2.translation() ;
    Quaterniond qq(pose2.rotation()) ;

    r.openGripper() ;
    ros::Duration(0.5).sleep() ;

     cout << r.isGripperOpen() << endl ;

    if ( r.planTipCartesian(Vector3d(0, 0.0, 0.2), plan) )
        r.execute(plan) ;

    r.closeGripper() ;
    cout << r.isGripperOpen() << endl ;
}


int main(int argc, char *argv[])
{
    ros::init(argc, argv, "planning") ;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

//    test1() ;
    test2();


    ros::waitForShutdown();
    spinner.stop();
}

